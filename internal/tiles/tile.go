package tiles

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/utils"
)

type Tile struct {
	Texture    *sdl.Texture
	Foreground *utils.RGBColor
	Rect       sdl.Rect
}

func CreateTile(texture *sdl.Texture, color *utils.RGBColor, rect sdl.Rect) *Tile {
	tile := &Tile{
		Texture:    texture,
		Rect:       rect,
		Foreground: color,
	}
	return tile
}

func (tile *Tile) Render(renderer *sdl.Renderer, dst sdl.Rect, Background *utils.RGBColor) {
	if Background != nil {
		renderer.SetDrawColor(Background.Red, Background.Green, Background.Blue, Background.Alpha)
		renderer.FillRect(&dst)
	}

	if tile.Foreground != nil {
		tile.Texture.SetAlphaMod(tile.Foreground.Alpha)
		tile.Texture.SetColorMod(tile.Foreground.Red, tile.Foreground.Green, tile.Foreground.Blue)
	}
	renderer.Copy(tile.Texture, &tile.Rect, &dst)
}
