package tiles

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/utils"
)

type TileSet struct {
	Name      string
	Author    string
	TileAtlas []TileAtlas
	Texture   []*sdl.Texture
	Tiles     map[string]*Tile
}

type TileAtlas struct {
	Filepath string
	TileSize struct {
		Height int32
		Width  int32
	}
	TileInfo []TileInfo
}

type TileInfo struct {
	Id    string
	Point struct {
		X int32
		Y int32
	}
	Fg string
}

func (tileset *TileSet) UnmarshalJSON(p []byte) error {
	var tmp map[string]interface{}
	if err := json.Unmarshal(p, &tmp); err != nil {
		return err
	}
	tileset.Name = tmp["name"].(string)
	tileset.Author = tmp["author"].(string)
	tmp2 := tmp["tileatlas"].([]interface{})
	for _, v := range tmp2 {
		tileatlas := TileAtlas{}
		tmp_tileatlas := v.(map[string]interface{})
		tileatlas.Filepath = tmp_tileatlas["filepath"].(string)
		tmp_tilesize := tmp_tileatlas["tile_size"].(map[string]interface{})
		tileatlas.TileSize = struct {
			Height int32
			Width  int32
		}{
			Height: int32(tmp_tilesize["height"].(float64)), Width: int32(tmp_tilesize["width"].(float64)),
		}
		tmp_tileinfo := tmp_tileatlas["tile_info"].([]interface{})
		for _, v2 := range tmp_tileinfo {
			tileinfo := TileInfo{}
			tmp_tileinfo := v2.(map[string]interface{})
			tileinfo.Id = tmp_tileinfo["id"].(string)
			tileinfo.Fg = tmp_tileinfo["fg"].(string)
			tmp_point := tmp_tileinfo["point"].(map[string]interface{})
			tileinfo.Point = struct {
				X int32
				Y int32
			}{
				X: int32(tmp_point["x"].(float64)),
				Y: int32(tmp_point["y"].(float64)),
			}
			tileatlas.TileInfo = append(tileatlas.TileInfo, tileinfo)
		}
		tileset.TileAtlas = append(tileset.TileAtlas, tileatlas)
	}
	return nil
}

func CreateTileSet(infoFilePath string, colors []utils.RGBColor, renderer *sdl.Renderer) (*TileSet, error) {
	tileSet := &TileSet{}
	tileSet.Tiles = make(map[string]*Tile)
	data, err := ioutil.ReadFile(infoFilePath)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &tileSet)
	if err != nil {
		return nil, err
	}
	for _, atlas := range tileSet.TileAtlas {
		texture, err := utils.LoadTexture(atlas.Filepath, renderer)
		if err != nil {
			return nil, err
		}
		tileSet.Texture = append(tileSet.Texture, texture)
		for _, tile_info := range atlas.TileInfo {
			var color *utils.RGBColor
			if tile_info.Fg != "" {
				flag := false
				for _, v := range colors {
					if tile_info.Fg == v.Id {
						color = &v
						flag = true
						break
					} else {
						flag = false
					}
				}
				if !flag {
					return nil, fmt.Errorf("Tileset: %s load fail.\tColor: %s not found!", tileSet.Name, tile_info.Fg)
				}
			}
			tile := CreateTile(texture, color, sdl.Rect{X: int32(atlas.TileSize.Width * tile_info.Point.X), Y: int32(atlas.TileSize.Height * tile_info.Point.Y), H: atlas.TileSize.Height, W: atlas.TileSize.Width})
			tileSet.Tiles[tile_info.Id] = tile
		}
	}
	return tileSet, nil
}
