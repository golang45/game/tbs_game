package config

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	ConfigFile string `json:_`
	Debug      bool   `json:debug`
	Graphics   struct {
		Resolution struct {
			Width  int32 `json:width`
			Height int32 `json:height`
		} `json:resolution`
		IsFullscreen bool  `json:is_fullscreen`
		FPS          int32 `json:fps`
	} `json:graphics`
}

func NewConfig(configFile string) (*Config, error) {
	config := Config{}
	config.ConfigFile = configFile
	err := config.load()

	return &config, err
}

func (config *Config) load() error {
	data, err := ioutil.ReadFile(config.ConfigFile)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &config)
	return err
}

func (config *Config) Save() error {
	data, err := json.Marshal(config)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(config.ConfigFile, data, 0644)
	return err
}
