package app

import (
	"errors"
	"log"
	"path/filepath"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/config"
	"gitlab.com/golang45/game/tbs_game/internal/manager"
	"gitlab.com/golang45/game/tbs_game/internal/scenes"
)

type App struct {
	Config       *config.Config
	AssetManager *manager.AssetManager
	InputManager *manager.InputManager
	Scene        scenes.Scene
	IsRunning    bool
	Window       *sdl.Window
	Renderer     *sdl.Renderer
}

func CreateApp(configFile string) (*App, error) {
	app := &App{}

	var err error
	app.Config, err = config.NewConfig(configFile)
	if err != nil {
		return nil, err
	}

	if err = app.sdlLibInit(); err != nil {
		return nil, err
	}

	if err = app.createWindow(); err != nil {
		return nil, err
	}

	app.AssetManager = manager.CreateAssetManager()
	if err = app.AssetManager.LoadColor(); err != nil {
		return nil, err
	}

	if err = app.AssetManager.LoadTileSet(filepath.Join(".", "assets", "tileset", "base"), app.Renderer); err != nil {
		return nil, err
	}

	app.InputManager, err = manager.CreateInputManager(filepath.Join(".", "config", "input_config.json"))
	if err != nil {
		return nil, err
	}

	app.Scene = &scenes.MainMenu{}
	app.Scene.Init()

	app.IsRunning = true

	return app, nil
}

func (app *App) sdlLibInit() error {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		return err
	}

	if err := img.Init(img.INIT_JPG | img.INIT_PNG); err != nil {
		return err
	}
	return nil
}

func (app *App) createWindow() error {
	var err error

	app.Window, err = sdl.CreateWindow("TBS GAME", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		app.Config.Graphics.Resolution.Width, app.Config.Graphics.Resolution.Height, sdl.WINDOW_SHOWN)
	if err != nil {
		return err
	}

	app.Renderer, err = sdl.CreateRenderer(app.Window, -1, sdl.RENDERER_ACCELERATED|sdl.RENDERER_PRESENTVSYNC)
	if err != nil {
		return err
	}
	return nil
}

func (app *App) Loop() {
	for app.IsRunning {
		app.update()
		app.draw()
	}
}

func (app *App) update() error {
	app.InputManager.Update()
	if app.InputManager.IsKeyPressed("Exit") {
		app.IsRunning = false
	}
	if app.Scene != nil {
		if err := app.Scene.Update(app.InputManager); err != nil {
			return err
		}
	} else {
		return errors.New("Scene not found")
	}
	return nil
}

func (app *App) draw() {
	app.Renderer.SetDrawColor(0, 0, 0, 255)
	app.Renderer.Clear()
	if app.Scene != nil {
		app.Scene.Draw(app.Renderer, app.AssetManager)
	}
	app.Renderer.Present()
}

func (app *App) Quit() {
	app.AssetManager.Destroy()
	app.Window.Destroy()
	app.Renderer.Destroy()
	sdl.Quit()
}

func Run(configFile string) {
	app, err := CreateApp(configFile)
	if err != nil {
		log.Fatalln(err)
	}
	app.Loop()
	app.Quit()
}
