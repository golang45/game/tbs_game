package scenes

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/manager"
)

type MainMenu struct {
}

func (mainMenu *MainMenu) Init() {}

func (mainMenu *MainMenu) Update(inputManager *manager.InputManager) error {
	return nil
}

func (mainMenu *MainMenu) Draw(renderer *sdl.Renderer, assetManager *manager.AssetManager) {}

func (mainMenu *MainMenu) Destroy() {}
