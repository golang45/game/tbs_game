package scenes

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/manager"
)

type Scene interface {
	Init()
	Update(inputManager *manager.InputManager) error
	Draw(renderer *sdl.Renderer, assetManager *manager.AssetManager)
	Destroy()
}
