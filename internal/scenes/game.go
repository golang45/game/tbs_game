package scenes

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/manager"
)

type GlobalMap struct {
}

func (globalMap *GlobalMap) Init() {}

func (globalMap *GlobalMap) Update(inputManager *manager.InputManager) error {
	return nil
}

func (globalMap *GlobalMap) Draw(renderer *sdl.Renderer, assetManager *manager.AssetManager) {}

func (globalMap *GlobalMap) Destroy() {}
