package manager

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/tiles"
	"gitlab.com/golang45/game/tbs_game/internal/utils"
)

type AssetManager struct {
	textures       []*sdl.Texture
	currentTileSet *tiles.TileSet
	tileSets       map[string]*tiles.TileSet
	colors         []utils.RGBColor
}

func CreateAssetManager() *AssetManager {
	assetmanager := &AssetManager{}
	assetmanager.tileSets = make(map[string]*tiles.TileSet)
	return assetmanager
}

func (assetManager *AssetManager) SetTileSet(name string) error {
	if v, ok := assetManager.tileSets[name]; ok {
		assetManager.currentTileSet = v
		return nil
	}
	return fmt.Errorf("SetCurrentTileSet: %s not found!", name)
}

func (assetManager *AssetManager) GetCurrentTileSet() *tiles.TileSet {
	return assetManager.currentTileSet
}

func (assetManager *AssetManager) GetColors() []utils.RGBColor {
	return assetManager.colors
}

func (assetManager *AssetManager) LoadColor() error {
	data, err := ioutil.ReadFile(filepath.Join(".", "assets", "color.json"))
	if err != nil {
		return err
	}
	return json.Unmarshal(data, &assetManager.colors)
}

func (assetManager *AssetManager) LoadTextures(filePaths []string, renderer *sdl.Renderer) error {
	for _, filePath := range filePaths {
		tex, err := utils.LoadTexture(filePath, renderer)
		if err != nil {
			return err
		}
		assetManager.textures = append(assetManager.textures, tex)
	}
	return nil
}

func (assetManager *AssetManager) LoadTileSet(dirpath string, renderer *sdl.Renderer) error {
	infoFilePath := filepath.Join(dirpath, "info.json")
	if err := utils.IsFileExist(infoFilePath); err != nil {
		return err
	}
	tileSet, err := tiles.CreateTileSet(infoFilePath, assetManager.colors, renderer)
	if err != nil {
		return err
	}
	assetManager.tileSets[tileSet.Name] = tileSet
	return nil
}

func (assetManager *AssetManager) Destroy() {
	for _, texture := range assetManager.textures {
		texture.Destroy()
	}
	for _, tileset := range assetManager.tileSets {
		for _, texture := range tileset.Texture {
			texture.Destroy()
		}
	}
}
