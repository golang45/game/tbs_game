package manager

import (
	"encoding/json"
	"io/ioutil"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/golang45/game/tbs_game/internal/utils"
)

type InputManager struct {
	configPath    string `json:-`
	mousePosition *struct {
		X       int32 `json:-`
		Y       int32 `json:-`
		OffsetX int32 `json:-`
		OffsetY int32 `json:-`
	} `json:-`
	mouseWheel *struct {
		X int32 `json:-`
		Y int32 `json:-`
	} `json:-`
	isLeftMousePress   bool   `json:-`
	isRigthMousePress  bool   `json:-`
	isMiddleMousePress bool   `json:-`
	KeyMap             []*Key `json:keymap`
}

type Key struct {
	Name      string      `json:name`
	Key       sdl.Keycode `json:key`
	isPressed bool        `json:-`
}

func CreateInputManager(configPath string) (*InputManager, error) {
	inputManager := &InputManager{}
	inputManager.configPath = configPath
	if err := utils.IsFileExist(configPath); err != nil {
		inputManager.defaultInputManager()
		return inputManager, inputManager.Save()
	} else {
		err = inputManager.load()
		return inputManager, err
	}
}

func (inputManager *InputManager) defaultInputManager() {
	inputManager.KeyMap = append(inputManager.KeyMap,
		&Key{"Exit", sdl.K_ESCAPE, false})
}

func (inputManager *InputManager) load() error {
	data, err := ioutil.ReadFile(inputManager.configPath)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &inputManager)
	return err
}

func (inputManager *InputManager) Save() error {
	data, err := json.Marshal(inputManager)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(inputManager.configPath, data, 0644)
	return err
}

func (inputManager *InputManager) Update() {
	for _, key := range inputManager.KeyMap {
		key.isPressed = false
	}
	inputManager.isLeftMousePress = false
	inputManager.isRigthMousePress = false
	inputManager.isMiddleMousePress = false

	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			break
		case *sdl.KeyboardEvent:
			keyCode := t.Keysym.Sym
			for _, key := range inputManager.KeyMap {
				if keyCode == key.Key {
					key.isPressed = true
				}
			}
		case *sdl.MouseButtonEvent:
			if t.State == sdl.PRESSED {
				switch t.Button {
				case 1:
					inputManager.isLeftMousePress = true
				case 2:
					inputManager.isMiddleMousePress = true
				case 3:
					inputManager.isRigthMousePress = true
				}
			}
		case *sdl.MouseMotionEvent:
			inputManager.mousePosition = &struct {
				X       int32 `json:-`
				Y       int32 `json:-`
				OffsetX int32 `json:-`
				OffsetY int32 `json:-`
			}{
				X: t.X, Y: t.Y, OffsetX: t.XRel, OffsetY: t.YRel,
			}
		case *sdl.MouseWheelEvent:
			inputManager.mouseWheel = &struct {
				X int32 `json:-`
				Y int32 `json:-`
			}{X: t.X, Y: t.Y}
		}
	}
}

func (inputManager *InputManager) IsKeyPressed(name string) bool {
	for _, key := range inputManager.KeyMap {
		if key.Name == name {
			return key.isPressed
		}
	}
	return false
}

func (inputManager *InputManager) IsMouseLeftPressed() bool {
	return inputManager.isLeftMousePress
}

func (inputManager *InputManager) IsMouseRightPressed() bool {
	return inputManager.isRigthMousePress
}

func (inputManager *InputManager) IsMouseMiddlePressed() bool {
	return inputManager.isMiddleMousePress
}

func (inputManager *InputManager) GetMousePosition() (X int32, Y int32) {
	return inputManager.mousePosition.X, inputManager.mousePosition.Y
}
