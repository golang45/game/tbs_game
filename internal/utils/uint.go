package utils

import "math/rand"

func RandUnit8(min, max int) uint8 {
	return uint8(rand.Intn(max-min) + min)
}
