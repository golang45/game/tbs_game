package utils

import (
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

func LoadTexture(filepath string, renderer *sdl.Renderer) (*sdl.Texture, error) {
	tempSurface, err := img.Load(filepath)
	if err != nil {
		return nil, err
	}

	tex, err := renderer.CreateTextureFromSurface(tempSurface)
	return tex, err
}
