package utils

type RGBColor struct {
	Id    string `json:id`
	Alpha uint8  `json:alpha`
	Red   uint8  `json:red`
	Green uint8  `json:green`
	Blue  uint8  `json:blue`
}
