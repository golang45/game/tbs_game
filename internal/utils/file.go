package utils

import (
	"errors"
	"os"
)

func IsFileExist(filepath string) error {
	if _, err := os.Stat(filepath); errors.Is(err, os.ErrNotExist) {
		return err
	}
	return nil
}
