package ecs

import "reflect"

type ECSManager struct {
	systems map[reflect.Type]System
}

func CreateECSManager() *ECSManager {
	ecsManager := &ECSManager{}
	ecsManager.systems = make(map[reflect.Type]System)
	return ecsManager
}

func (ecsManager *ECSManager) AddSystem(system System) {
	ecsManager.systems[reflect.TypeOf(system)] = system
}

func (ecsManager *ECSManager) RemoveSystem(system System) {
	delete(ecsManager.systems, reflect.TypeOf(system))
}

func (ecsManager *ECSManager) Update() {
	for _, v := range ecsManager.systems {
		v.Update()
	}
}

func (ecsManager *ECSManager) AddEntityToSystem(entity *Entity) {
	for _, v := range ecsManager.systems {
		entityComponents := entity.GetComponents(v.GetFilter()...)
		flag := true
		for _, v2 := range entityComponents {
			if !v2.IsActive() {
				flag = false
			}
		}
		if flag {
			v.AddEntity(entity)
		}
	}
}

func (ecsManager *ECSManager) RemoveEntityToSystem(entity *Entity) {
	for _, v := range ecsManager.systems {
		v.RemoveEntity(entity)
	}
}
