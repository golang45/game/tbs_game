package ecs

type System interface {
	Init(ecsManager *ECSManager)
	AddEntity(entity *Entity)
	RemoveEntity(entity *Entity)
	GetFilter() []Component
	IsActive() bool
	Update()
}
