package ecs

import "reflect"

type Entity struct {
	active     bool
	components map[reflect.Type]Component
	parent     *Entity
	children   []*Entity
}

func CreateEntity(components ...Component) *Entity {
	entity := &Entity{}
	entity.active = true
	entity.components = make(map[reflect.Type]Component)
	for _, v := range components {
		entity.components[reflect.TypeOf(v)] = v
	}
	return entity
}

func (entity *Entity) AddComponent(component Component) {
	entity.components[reflect.TypeOf(component)] = component
}

func (entity *Entity) Addcomponents(components ...Component) {
	for _, v := range components {
		entity.components[reflect.TypeOf(v)] = v
	}
}

func (entity *Entity) RemoveComponent(component Component) {
	delete(entity.components, reflect.TypeOf(component))
}

func (entity *Entity) Removecomponents(components ...Component) {
	for _, v := range components {
		delete(entity.components, reflect.TypeOf(v))
	}
}

func (entity *Entity) GetComponent(component Component) Component {
	if v, ok := entity.components[reflect.TypeOf(component)]; ok {
		return v
	}
	return nil
}

func (entity *Entity) GetComponents(components ...Component) []Component {
	var ret []Component
	i := 0
	for _, component := range components {
		for _, entityCompoent := range entity.components {
			if reflect.TypeOf(component) == reflect.TypeOf(entityCompoent) {
				i++
				ret = append(ret, entityCompoent)
			}
		}
	}
	if i == len(components) {
		return ret
	}
	return nil
}

func (entity *Entity) AppendChildren(children *Entity) {
	children.parent = entity
	entity.children = append(entity.children, entity)
}

func (entity *Entity) IsActive() bool {
	return entity.active
}

func (entity *Entity) SetActive(active bool) {
	entity.active = active
}

func (entity *Entity) GetParent() *Entity {
	return entity.parent
}

func (entity *Entity) GetChildren() []*Entity {
	return entity.children
}
