package ecs

type Component interface {
	IsActive() bool
}
