package main

import (
	"gitlab.com/golang45/game/tbs_game/internal/app"
)

const CONFIGFILE = "c:/Program Files/Go/src/gitlab.com/golang45/game/tbs_game/config/game_config.json"

func main() {
	app.RunGame(CONFIGFILE)
}
