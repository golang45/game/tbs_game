package main

import (
	"gitlab.com/golang45/game/tbs_game/internal/app"
)

const CONFIGFILE = "./config/game_config.json"

func main() {
	app.Run(CONFIGFILE)
}
