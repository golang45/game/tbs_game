PROJECTNAME:=$(shell basename "$(PWD)")
OS=$(shell uname -s)

GOBASE=$(shell pwd)
GOPATH="$(GOBASE)/vendor:$(GOBASE)"
GOBIN=$(GOBASE)/bin
GOFILES:=$(GOBASE)/cmd/tbs_game

ifeq ($(OS),Windows_NT)
	PROJECTNAME:=$(PROJECTNAME)_win.exe
	GOFILES:=$(GOFILES)_win
else
	PROJECTNAME:=$(PROJECTNAME)_linux
	GOFILES:=$(GOFILES)_linux
endif

STDERR=./tmp/.$(PROJECTNAME)-stderr.txt

os:
	@echo $(OS)

## compile: Compile the binary.
compile:
	@-touch $(STDERR)
	@-$(MAKE) -s go-compile 2> $(STDERR)
	@cat $(STDERR) | sed -e '1s/.*/\nError:\n/'  | sed 's/make\[.*/ /' | sed "/^/s/^/     /" 1>&2

## tidy: Update dependance.
tidy:
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go mod tidy

## fmt FOLDER=: Fmt to folder
fmt:
	@echo "check fmt to folder: $(FOLDER)"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go fmt $(FOLDER)

go-compile: go-mod-download go-build

go-build:
	@echo "	! Build binary !"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go build -gcflags="-N -l" -o $(PROJECTNAME) $(GOFILES)
	@echo "	! Build binary finish !"

go-mod-download:
	@echo "	! Download dependance !"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go mod download
	@echo "	! Download dependance finish !"

.PHONY: help
all: help
help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo